# Software Studio 2018 Spring Assignment 01 Web Canvas

Just a basic canvas with basic component.

You can use the pen tool and the eraser tool to draw and erase things.

Also you can adjust the color and the width of the brush.

The second row contains the Typing Function.

By pressing the "輸入文字", you can tell the canvas what you want to write.

Then, you can click wherever you want on the canvas to actually print some words.

BTW, you can also change the size and the font.



## Code description:

There's one variable called "currentTool" to store what tool is being used at this moment;

and by pressing the pen tool, the eraser tool, and the text tool, the variable would update automatically.

With the variable "currentTool", we are able to tell the program what should it do.

If we are using the pen tool, the program just need to track the mouseposition and stroke on the path that we just pass through when the mousebutton is clicked.

If we are using the eraser tool, it simply change the brush color into white. (Because the default backround is white.)

About the text tool. I use one variable to store what the user wants to write.

Just like the stamp tool in other software, once we click on the canvas, it will prints some words onto it.

The reset button simply put a fullscreen, white rectangle on the canvas.

